# Plastic supply chain model

The plastic supply chain model has been developed by members of the EPSE group in the following paper:

> [Meys, R., Kätelhön, A., Bachmann, M., Winter, B., Zibunas, C., Suh, S., & Bardow, A. (2021). Achieving net-zero greenhouse gas emission plastics by a circular carbon economy. Science, 374(6563), 71-76](https://doi.org/10.1126/science.abg9853)

You can find the corresponding Git here: 

https://github.com/Bene94/Plastic-supply-chain-model-including-matlab-code-

https://zenodo.org/record/5118762#.Y4XwdH3MIuU
